import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NodeList extends Component {
  state = {

  };

  constructor (props) {
    super(props);
  }

  renderNotes (notes) {
    return notes.map((note, index) => (
      <div key={`key-${index}`} className="list-group-item list-group-item-action flex-column align-items-start">
        <div className="d-flex w-100 justify-content-between">
          <h5 className="mb-1"><Link className="nav-link p-0" to={`/notes/${note._id}`}>{note.title}</Link></h5>
          <small className="text-muted">3 days ago</small>
        </div>
        <p className="mb-1">{note.body}</p>
      </div>
    ));
  }

  renderNoNoteFound () {
    return (
      <div className="list-group-item list-group-item-action flex-column align-items-start">
        <div className="d-flex w-100 justify-content-between">
          No note found!!
        </div>
      </div>
    );
  }

  render () {
    const notes = Object.values(this.props.notes);

    return (
      <div className="list-group">
        {
          !!notes.length
            ? this.renderNotes(notes)
            : this.renderNoNoteFound()
        }
      </div>
    );
  }
}

export default NodeList;
