import React, { Component } from 'react';

class ShowPage extends Component {
  state = {};

  constructor (props) {
    super(props);
  }

  render () {
    const { title, body } = this.props.note;
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{body}</p>
        </div>
      </div>
    );
  }
}

export default ShowPage;
