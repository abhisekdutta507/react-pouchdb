import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class NewPage extends Component {
  state = {
    note: {
      title: '',
      body: '',
      createdAt: Date.now(),
      updatedAt: Date.now()
    }
  };

  constructor (props) {
    super(props);
  }

  setNote = (e) => {
    const { note } = this.state;
    const { name, value } = e.target;
    
    // update the value
    note[name] = value;

    // update component state with updated value
    this.setState({ note });
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { note } = this.state;
    const { onSave } = this.props;

    note.createdAt = note.updatedAt = Date.now();
    onSave(note);
  }

  render () {
    const { title, body } = this.state.note;
    return (
      <div className="card">
        <div className="card-body">
          <h5 className={'card-title'}>New Note</h5>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label htmlFor="title">Title</label>
              <input type="text" className="form-control" id="title" name="title" value={title} onChange={this.setNote}></input>
            </div>
            <div className="form-group">
              <label htmlFor="body">Body</label>
              <textarea className="form-control" id="body" rows="3" name="body" value={body} onChange={this.setNote}></textarea>
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
            {/* <div className={'note-form-buttons'}>
              <button className={'btn'}>Submit</button>
              <Link to="/">Cancel</Link>
            </div> */}
          </form>
        </div>
      </div>
    );
  }
}

export default NewPage;
