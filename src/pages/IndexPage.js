import React, { Component } from 'react';
import NodeList from '../components/NodeList';

class IndexPage extends Component {
  state = {

  };

  constructor (props) {
    super(props);
  }

  render () {
    const { notes } = this.props;

    return (
      <div className="list-wrapper">
        <h5 className={'list-form-heading'}>Notes</h5>
        <NodeList notes={notes} />
      </div>
    );
  }
}

export default IndexPage;
