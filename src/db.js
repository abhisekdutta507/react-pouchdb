import PouchDB from 'pouchdb';

class DB {
  constructor(name) {
    this.db = new PouchDB(name);
  }

  async getAllNotes () {
    const allNotes = await this.db.allDocs({ include_docs: true });
    
    let notes = {};
    allNotes.rows.forEach((n) => notes[n.id] = n.doc);

    return notes;
  }

  async createNote (note) {
    note.createdAt = note.updatedAt = Date.now();
    
    // create the note in pouchdb
    return await this.db.post({ ...note });
  }
}

export default DB;
