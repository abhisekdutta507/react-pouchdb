import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';

import Spinner from './assets/Spinner.svg';

import NavBar from './components/NavBar';

import IndexPage from './pages/IndexPage';
import ShowPage from './pages/ShowPage';
import NewPage from './pages/NewPage';

import DB from './db';

class App extends Component {
  state = {
    db: new DB(''),
    notes: {},
    loading: true
  };

  constructor (props) {
    super(props);
  }

  async componentDidMount () {
    const notes = await this.state.db.getAllNotes();
    
    // update the notes from pouch db
    this.setState({ notes, loading: false });
  }

  onSave = async (note) => {
    const { db, notes } = this.state;
    const { id } = await db.createNote(note);

    note._id = `${id}`;
    notes[note._id] = note;

    this.setState({ notes })
  }

  render () {
    const { notes, loading } = this.state;

    return (
      <BrowserRouter>
        <div className="App">
          <NavBar />
          {
            loading
              ? <img src={Spinner} alt="spinner" className="spinner" />
              : <>
                <Route exact path="/" component={(props) => <IndexPage {...props} notes={notes} />} />
                <Route exact path="/notes/:_id" component={(props) => <ShowPage {...props} note={notes[props.match.params._id]} />} />
                <Route exact path="/new" component={(props) => <NewPage {...props} onSave={this.onSave} />} />
              </>
          }
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
